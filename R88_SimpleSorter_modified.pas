{
  Ruddy88's VIS-G Auto Patcher - modified by hat
  FALLOUT 4
  
  Aim: This script utility will automatically add all valid ARMO items for DEF_UI's tag system to a single patch ESP.
  The items will attempt to auto categorise themselves, similar to Valdacils Item Sorting, though with simpler categorisation.
  
  Requires: FO4Edit, MXPF, DEFUI and DEF_UI Iconlibs Rescaled and Fixed.
  OPTIONAL: AWKCR and HUDFramework.
  + Core files from Ruddy88's Simple Sorter Nexus page.
  
  Credits: 
  MatorTheEternal (For MXPF and helping me throughout my entire modding life so far)
  Zilav - For helping with several roadblocks
  Neeanka - For DEFUI
  Valdacil - For VIS and all his work on the various other tools.
  The lads from Gambit's discord server (helping me organise categories and explaining some finer points of DEFUI).
  Pra - For his Visify scripts that perform similar functions. Was able to work out some missing categorisation ideas from his scripts.
  
  Many others from many discords, too many to name.
  
  <==========================================================>
  
  And from hat - I'd like to thank Ruddy88 himself. My armor sorter would be nothing without his script and mod.
  }

unit UserScript;
// Import MXPF functions
uses 'lib\mxpf';

const
  localFormID_ArmoAEC = $000851;
  localFormID_WeapAEC = $00085D;
  localFormID_AmmoAEC = $00095A;
  localFormID_ExplAEC = $000BFD;
  localFormID_OthrAEC = $000860;
  
var
  UserCancelled, bReset, bAWKCR, bWeightless, bVisibleComp: Boolean;
  bAlch, bAmmo, bBook, bKeym, bMisc, bNote, bArmorOnly, bMod, bSkipArmoWeap, bScrapMarker: Boolean;
  sTag, sStr, sName, sCaption: String;
  tlAlchFLTR, tlFoodKWRD, tlDrinkKWRD, tlChemsKWRD, tlStealthboyKWRD, tlDrinkSND, tlChemsSND, tlChemsINCL, tlMineKWRD: TStringList;
  tlWeapArmoINNR, tlAmmoMISC, tlCurrencyMISC, tlNoteMISC, tlCollectibleMISC, tlMeleeANIM, tlGunANIM, tlPatched, tlAidText: TStringList;
  cArmoAEC, cAmmoAEC, cWeapAEC, cExplAEC, cOthrAEC, fArmorKeywords, dnClothes: IInterface;
  iRatio: Integer;
  

// Options dialogue form prep
procedure PrepareDialog(frm: TForm; caption: String; height, width: Integer);
begin
  frm.BorderStyle := bsDialog;
  frm.Height := height;
  frm.Width := width;
  frm.Position := poScreenCenter;
  frm.Caption := caption;
end;

// Dialogue form for choosing which plugins to include in the patch. Taken from mteFunctions
function MultipleFileSelectString(sPrompt: String; var sFiles: String): Boolean;
var
  sl: TStringList;
begin
  sl := TStringList.Create;
  try
    Result := MultipleFileSelect(sl, (sPrompt));
    sFiles := sl.CommaText;
  finally
    sl.Free;
  end;
end;

// Another part of the dialogie form. Has been edited to exclude vanilla esms and SkyAI plugins from checkable options.
function MultipleFileSelect(var sl: TStringList; prompt: string): Boolean;
const
  spacing = 24;
var
  frm: TForm;
  pnl: TPanel;
  lastTop, contentHeight: Integer;
  cbArray: Array[0..4351] of TCheckBox;
  lbl, lbl2: TLabel;
  sb: TScrollBox;
  i: Integer;
  f: IInterface;
  sFileName: String;
begin
  Result := false;
  frm := TForm.Create(nil);
  try
    frm.Position := poScreenCenter;
    frm.Width := 300;
    frm.Height := 600;
    frm.BorderStyle := bsDialog;
    frm.Caption := '::: PLUGIN SELECTION :::';
    
    // create scrollbox
    sb := TScrollBox.Create(frm);
    sb.Parent := frm;
    sb.Align := alTop;
    sb.Height := 500;
    
    // create label
    lbl := TLabel.Create(sb);
    lbl.Parent := sb;
    lbl.Caption := prompt;
    lbl.Font.Style := [fsBold];
    lbl.Left := 8;
    lbl.Top := 10;
    lbl.Width := 270;
    lbl.WordWrap := true;
    lbl2 := TLabel.Create(sb);
    lbl2.Parent := sb;
    lbl2.Caption := sCaption;
    lbl2.Font.Style := [fsItalic];
    lbl2.Left := 8;
    lbl2.Top := lbl.Top + lbl.Height + 12;
    lbl2.Width := 250;
    lbl2.WordWrap := true;
    lastTop := lbl2.Top + lbl2.Height + 12 - spacing;
    
    // create checkboxes
    for i := 0 to FileCount - 2 do begin
      f := FileByLoadOrder(i);
      sFileName := (GetFileName(f));
      if (GetAuthor(f) = 'R88_SimpleSorter') then
        Continue;
      cbArray[i] := TCheckBox.Create(sb);
      cbArray[i].Parent := sb;
      cbArray[i].Caption := Format(' [%s] %s', [IntToHex(i, 2), GetFileName(f)]);
      cbArray[i].Top := lastTop + spacing;
      cbArray[i].Width := 260;
      lastTop := lastTop + spacing;
      cbArray[i].Left := 12;
      //if bReset then
        cbArray[i].Checked := true
      //else
      //  cbArray[i].Checked := sl.IndexOf(GetFileName(f)) > -1;
      //if (sFilename = 'ArmorKeywords.esm') and bAWKCR then
      //  begin
      //    cbArray[i].Checked := true;
      //    cbArray[i].Enabled := False;
      //  end;
    end;
    
    contentHeight := spacing*(i + 2) + 150;
    if frm.Height > contentHeight then
      frm.Height := contentHeight;
    
    // create modal buttons
    cModal(frm, frm, frm.Height - 70);
    sl.Clear;
    
    if frm.ShowModal = mrOk then begin
      Result := true;
      for i := 0 to FileCount - 2 do begin
        f := FileByLoadOrder(i);
        sFileName := (GetFileName(f));
        if (GetAuthor(f) = 'R88_SimpleSorter') then
          Continue
        else if (cbArray[i].Checked) and (sl.IndexOf(GetFileName(f)) = -1) then
          sl.Add(GetFileName(f));
      end;
    end;
  finally
    frm.Free;
  end;
end;


// Creates various TStringLists, mostly used in condition checking.
procedure createLists();
begin

  tlAlchFLTR := TStringList.create;
    tlAlchFLTR.add('HC_EffectType_Adrenaline');
    tlAlchFLTR.add('HC_EffectType_Disease');
    tlAlchFLTR.add('HC_IconColor_Red');
    tlAlchFLTR.add('HC_EffectType_Hunger');
    tlAlchFLTR.add('HC_EffectType_Sleep');
    tlAlchFLTR.add('HC_EffectType_Thirst');
    
  tlFoodKWRD := TStringList.create;
    tlFoodKWRD.add('FoodEffect');
    tlFoodKWRD.add('HC_DiseaseRisk_FoodVeryHigh');
    tlFoodKWRD.add('HC_DiseaseRisk_FoodLow');
    tlFoodKWRD.add('HC_DiseaseRisk_FoodHigh');
    tlFoodKWRD.add('HC_DiseaseRisk_FoodStandard');
    tlFoodKWRD.add('FruitOrVegetable');
    tlFoodKWRD.add('ObjectTypeFood');
  
  tlDrinkKWRD := TStringList.create;
    tlDrinkKWRD.add('ObjectTypeWater');
    tlDrinkKWRD.add('ObjectTypeDrink');
    tlDrinkKWRD.add('ObjectTypeNukaCola');
  
  tlChemsKWRD := TStringList.create;
    tlChemsKWRD.add('ObjectTypeStimpak');
    tlChemsKWRD.add('ObjectTypeChem');
    tlChemsKWRD.add('CA_ObjType_ChemBad');
    tlChemsKWRD.add('HC_CausesImmunodeficiency');
    tlChemsKWRD.add('HC_SustenanceType_IncreasesHunger');
   
  tlStealthboyKWRD := TStringList.Create;
    tlStealthboyKWRD.add('ChemTypeStealthBoy');
    
  tlDrinkSND := TStringList.Create;
    tlDrinkSND.add('NPCHumanDrinkGeneric');
    tlDrinkSND.add('NPCHumanDrinkNukaCola');
    tlDrinkSND.add('DLC03NPCHumanDrinkSludgePack');
    tlDrinkSND.add('DLC03NPCHumanDrinkVIM');
    tlDrinkSND.add('NPCHumanDrinkWaterFountainStart');
    tlDrinkSND.add('DLC03NPCHumanChemsSludgePack');
    
  tlChemsSND := TStringList.create;
    tlChemsSND.add('NPCHumanEatMentats');
    tlChemsSND.add('NPCHumanChemsPsycho');
    tlChemsSND.add('NPCHumanChemsUseJet');
    tlChemsSND.add('NPCHumanChemsFury');
    tlChemsSND.add('NPCHumanChemsAddictol');
    tlChemsSND.add('NPCHumanChemsRadaway');
    tlChemsSND.add('NPCHumanChemsRadX');
    tlChemsSND.add('NPCHumanChemsStimpak');
    tlChemsSND.add('NPCHumanChemsUseJetFuel');
  
  tlChemsINCL := TStringList.create;
    tlChemsINCL.add('HC_Antibiotics_SILENT_SCRIPT_ONLY');
    tlChemsINCL.add('HC_Herbal_Anodyne');
    tlChemsINCL.add('HC_Herbal_Antimicrobial');
    tlChemsINCL.add('HC_Herbal_Stimulant');
    
    
  tlMineKWRD := TStringList.Create;
    tlMineKWRD.add('WeaponTypeMine');
    tlMineKWRD.add('WeaponTypePulseMine');
    tlMineKWRD.add('WeaponTypePlasmaMine');
    tlMineKWRD.add('WeaponTypeBottlecapMine');
    tlMineKWRD.add('WeaponTypeNukaMine');
    tlMineKWRD.add('WeaponTypeCryoMine');
    tlMineKWRD.add('AnimsMine');
    
  tlWeapArmoINNR := TStringList.Create;
    tlWeapArmoINNR.add('DLC01dn_LightningGun');
    tlWeapArmoINNR.add('DLC01dn_PowerArmor');
    tlWeapArmoINNR.add('DLC03_dn_CommonArmor');
    tlWeapArmoINNR.add('DLC03_dn_CommonGun');
    tlWeapArmoINNR.add('DLC03_dn_CommonMelee');
    tlWeapArmoINNR.add('DLC03_dn_Legendary_Armor');
    tlWeapArmoINNR.add('DLC03_dn_Legendary_Weapons');
    tlWeapArmoINNR.add('DLC04_dn_CommonArmorUpdate');
    tlWeapArmoINNR.add('DLC04_dn_CommonGunUpdate');
    tlWeapArmoINNR.add('DLC04_dn_CommonMeleeUpdate');
    tlWeapArmoINNR.add('dn_Clothes');
    tlWeapArmoINNR.add('dn_CommonArmor');
    tlWeapArmoINNR.add('dn_CommonGun');
    tlWeapArmoINNR.add('dn_CommonMelee');
    tlWeapArmoINNR.add('dn_DLC04_PowerArmor_Overboss');
    tlWeapArmoINNR.add('dn_PowerArmor');
    tlWeapArmoINNR.add('dn_VaultSuit');
    tlWeapArmoINNR.add('dn_DLC04_PowerArmor_NukaCola');
    tlWeapArmoINNR.add('dn_DLC04_PowerArmor_Quantum');
  
  tlAmmoMISC := TStringList.create;
    tlAmmoMISC.add('DLC05FireworkTransform');
    tlAmmoMISC.add('DLC05FireworkFlareTransform');
  
  tlCurrencyMISC := TStringList.create;
    tlCurrencyMISC.add('token');
    tlCurrencyMISC.add('ticket');
    tlCurrencyMISC.add('Medallion');
    tlCurrencyMISC.add('Money');
    tlCurrencyMISC.add('Cash');
    tlCurrencyMISC.add('Dollar');
    
  tlNoteMISC := TStringList.create;
    tlNoteMISC.add('Note');
    tlNoteMISC.add('Schematic');
    tlNoteMISC.add('Recipe');
    tlNoteMISC.add('Newspaper');
    tlNoteMISC.add('Map');
    tlNoteMISC.add('Blueprint');
    tlNoteMISC.add('Flyer');
    tlNoteMISC.add('Holotape');
    tlNoteMISC.add('Formula');
    tlNoteMISC.add('Plans');   

  tlCollectibleMISC := TStringlist.create;
    tlCollectibleMISC.add('MiscModel');
    tlCollectibleMISC.add('MiscLunch');
    tlCollectibleMISC.add('Signed');
    tlCollectibleMISC.add('Model');
    
  tlMeleeANIM := TStringlist.create;
    tlMeleeANIM.add('HandToHandMelee');
    tlMeleeANIM.add('OneHandAxe');
    tlMeleeANIM.add('OneHandDagger');
    tlMeleeANIM.add('OneHandMace');
    tlMeleeANIM.add('OneHandSword');
    tlMeleeANIM.add('TwoHandAxe');
    tlMeleeANIM.add('TwoHandSword');
    
  tlGunANIM := TStringlist.create;
    tlGunANIM.add('Bow');
    tlGunANIM.add('Gun');
    tlGunANIM.add('Staff');
 
  tlAidText := TStringlist.create;
    tlAidText.add('Bandage');
    tlAidText.add('Trauma');
    tlAidText.add('First Aid');
    tlAidText.add('FirstAid');


  // Empty strings list to store patched INNR records so they aren't re patched. Forms are dynamically added during patching.
  tlPatched := TStringlist.create;
end;


//////////////////////////////
// Various Custom Functions //
//////////////////////////////

// Returns TRUE if a record contains any keyword from a stringlist of keywords.
function HasKeywordFromList(rec: IInterface; kwList: TStringList): boolean;
var
  kwda: IInterface;
  j: integer;
  kwd: String;
begin
  Result := false;
  kwda := ElementByPath(rec, 'KWDA');
  for j := 0 to ElementCount(kwda) - 1 do begin
    kwd := GetElementEditValues(WinningOverride(LinksTo(ElementByIndex(kwda, j))), 'EDID');
    if kwList.indexOf(kwd) >= 0 then begin
      Result := true;
      exit;
    end;
  end;
end;

// Courtesy of Pra's VISifier/TAGifier scripts
// Returns TRUE if a MISC record contains an effect at any index.
function hasEffect(e: IInterface; effectName: String): boolean;
var
  effects: IInterface;
  curEff, curWat: IInterface;
  i: Integer;
begin
  Result := false;
  effects := ElementByName(e, 'Effects');
  for i := 0 to ElementCount(effects)-1 do begin
    curEff := ElementByIndex(effects, i);  
    curWat := WinningOverride(LinksTo(ElementBySignature(curEff, 'EFID')));   
    if(GetElementEditValues(curWat, 'EDID') = effectName) then begin
      Result := true;
      exit;
    end;
  end;
end;

// Returns TRUE if a field contains any text from a list of string options.
function HasTextFromSList(rec: IInterface; sList: TStringList; path: String): boolean;
var
j: Integer;

begin
  Result := False;
  path := GetElementEditValues(rec, path);
  for j := 0 to sList.Count -1 do begin
    if ContainsText(path, sList[j]) then begin
      Result := true;
      Break;
    end;
  end;
end;


// Detect an already existing tag at start and delete it. trims spaces, covers [], (), {}, ||.
procedure DeleteExistingTag();
var
j: Integer;
bTagExists: Boolean;

begin
  bTagExists := True;
  // use while loop to repeat function in case of multiple tags
  While bTagExists do
  begin
    if sStr[1] = '(' then
      j := Pos(')', sStr)
    else if sStr[1] = '[' then
      j := Pos(']', sStr)
    else if sStr[1] = '{' then
      j := Pos('}', sStr)
    else if sStr[1] = '|' then
      // Gets next occuring pipe. If none exists then J will equal string length and will not be deleted anyway.
      for j := 2 to Length(sStr) do
        begin
          if (sStr[j] = '|') then
            Break;
        end
    else
      Exit;

    if (j <= 1) or (Length(sStr) <= j) then
      bTagExists := False
    else begin
      Delete(sStr, 1, j);
      sStr := Trim(sStr);
    end;
  end;
end;


procedure DeleteEndTag(rec:IInterface; sPath:String);
var
j, k, l, m: Integer;
begin
  sStr := GetElementEditValues(rec, sPath);
  k := Length(sStr);
  if (sStr[k] = '}') then begin
      l := 0;
      m := 0;
      for j := k downto 1 do
        if (sStr[j] = '}') then
          l := l+1
        else if (sStr[j] = '{') then begin
          m := m+1;
          if m = l then begin
            Delete(sStr, j, k);
            Break;
          end;
        end;
  end;
  sStr := Trim(sStr);
  SetElementEditValues(rec, sPath, sStr);
end;

Procedure AddComponentTags(rec:IInterface);
var
  j, k: Integer;
  m, n, o: IInterface;
  s, t: String;
begin
  m := ElementBySignature(rec, 'CVPA');
  j := ElementCount(m);
  for k := 0 to (j-1) do
    begin
      n := LinksTo(ElementByName(ElementByIndex(m, k), 'Component'));
      s := GetElementEditValues(rec, 'FULL');
      t := GetElementEditValues(n, 'FULL');
      if (j = 1) then
        SetElementEditValues(rec, 'FULL', (s +'{{{'+t+'}}}'))
      else if (k = 0) then
        SetElementEditValues(rec, 'FULL', (s+'{{{'+t))
      else if (k = j-1) then
        SetElementEditValues(rec, 'FULL', (s+', '+t+'}}}'))
      else
        SetElementEditValues(rec, 'FULL', (s+', '+t));
    end;
end;

// Writes sTag string to beginning of existing string.
procedure AddTagString(sTag, sPath: String; rec: IInterface);
begin
    sStr := GetElementEditValues(rec, sPath);
    if (sStr <> '') then begin
      DeleteExistingTag();
      SetElementEditValues(rec, sPath, (sTag + ' ' + sStr));
    end;
end;

// Clears workbench and category keywords from recipes, adds AEC keywords instead.
procedure clearRecipeKeywords(rec, kWorkbench: IInterface);
begin
  Remove(ElementBySignature(rec, 'BNAM'));
  Add(rec, 'BNAM', false);
  SetElementEditValues(rec, 'BNAM - Workbench Keyword', Name(kWorkbench));
  if not ElementExists(rec, 'FNAM') then
    begin
      Add(rec, 'FNAM', false);
      SetElementEditValues(rec, 'FNAM - Category\Keyword', Name(cOthrAEC));
    end;
end;

// Checks an ARMO record and its linked ARMA records for any indication of being compatible with HumanRace. Several fields are checked.
function hasHumanRace(rec:IInterface):Boolean;
var
  m, n, p, q, r, s: IInterface;
  k, l: Integer;
begin
  result := true;
  if (GetElementEditValues(rec, 'RNAM - Race') = 'HumanRace "Human" [RACE:00013746]') then
      exit;
  m := ElementByName(rec, 'Models');
  for k := 0 to ElementCount(m)-1 do 
    begin
      n := ElementByIndex(m, k);
      p := WinningOverride(LinksTo(ElementBySignature(n, 'MODL')));
      if (GetElementEditValues(p, 'RNAM - Race') = 'HumanRace "Human" [RACE:00013746]') then
          exit;
      q := ElementByName(p, 'Additional Races');
      for l := 0 to ElementCount(q)-1 do
          if (GetEditValue(ElementByIndex(q, l)) = 'HumanRace "Human" [RACE:00013746]') then        // This is the part that isnt working
              exit;
    end;
  result := false;
end;

////////////////////////////
// GROUP Filter Functions //
////////////////////////////

// The following procedures filter out already loaded records prior to copying them to the plugin if they are deemed invalid for tagging.

procedure filterARMO(j: Integer; rec: IInterface);
begin
  if not ElementExists(rec, 'FULL - Name')
  then
    RemoveRecord(j);
end;

///////////////////////////
// GROUP Patch Functions //
///////////////////////////

// The following procedures modify the valid records after they have been copied to the plugin.

var
	i: Integer;
	tempString: String;

function getSlots(rec: IInterface):String;
begin
	Result := '';
	if (GetElementEditValues(rec, 'Record Header\Signature') = 'ARMO') then begin
		tempString := GetElementEditValues(rec, 'BOD2 - Biped Body Template\First Person Flags');
		for i := 1 to length(tempString) do begin
			if ord(tempString[i]) = 1 then begin
				Result := IntToStr((ord(tempString[i])) * (i + 29));
				exit;
			end;
		end;
	end;
end;

function patchARMO(rec: IInterface):Boolean;
begin
  if not hasHumanRace(rec)
  or HasKeyword(rec, 'playerCannotEquip') then
    sTag := '{Non-Human}'
  else if HasKeyword(rec, 'VaultSuitKeyword') then
    sTag := '(VaultSuit)'
  else if HasKeyword(rec, 'ArmorTypePower') then
    sTag := '[PowerArmor]'
  else
    begin
		sTag := '[' + getSlots(rec) + ']';
    end;

    Result := true;
end;

/////////////////
// Main Script //
/////////////////

function Initialize: Integer;

var
  i: Integer;
  rec: IInterface;
  sFiles: String;
  
begin

  // Call MXPF init functions and set MXPF prefs
  InitializeMXPF;
  mxLoadMasterRecords := true;
  mxSkipPatchedRecords := true;
  mxLoadWinningOverrides := true;
  mxDebug := false;
  mxSaveDebug := false;
  mxSaveFailures := false;
  mxPrintFailures := false;
  
  if not MultipleFileSelectString(('Select the plugins you would like to be included.'), sFiles) then
    exit;
  SetInclusions(sFiles);  
  PatchFileByAuthor('Ruddy88 (modified by hat)');
  
  ShowMessage('Script Initialising. Patching can take some time depending on the size of your mod list. Please be patient.');
  createLists();
  
  RemoveNode(GroupBySignature(mxPatchFile, 'ARMO'));

  // Load valid records.
  AddMessage('Loading records...');
  LoadRecords('ARMO');
      
  AddMessage('Filtering invalid records...');
  
  // Filter out unnecessary records on groups that may contain invalid records for tagging.
  for i := MaxrecordIndex downto 0 do begin
    rec := GetRecord(i);
    if (GetElementEditValues(rec, 'Record Header\Signature') = 'ARMO') then
      filterARMO(i, rec);
  end;
  
  // Copy remaining records to patch file.
  AddMessage('Copying records to patch...');
  AddMessage('This process can take several minutes, please be patient');
  CopyRecordsToPatch;
  
  // Patch Files
  AddMessage('Beginning patch file process...');
  AddMessage('Patching ' + IntToStr(MaxPatchRecordIndex + 1) + (' Records...'));
  for i :=  MaxPatchRecordIndex downto 0 do begin
    rec := GetPatchRecord(i);
    if (GetElementEditValues(rec, 'Record Header\Signature') = 'ARMO') then
      begin
        if patchARMO(rec) then
          AddTagString(sTag, 'FULL - Name', rec)
        else
          Remove(rec);
      end;
  end;
  
  AddMessage('Patching Process Complete. Finalising Script');
  AddMessage('This process can take several minutes, please be patient');
  
  // Cleanup MXPF
  CleanMasters(mxPatchFile);
  //PrintMXPFReport;
  FinalizeMXPF;
  ShowMessage('Patching Complete.');
end;
end.