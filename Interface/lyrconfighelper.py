import json
import os
import xml.etree.ElementTree as ET

BIGGEST_SUBFILTER = 0x80000
ARMOR_SLOT = 4
LYR_FILE = "lyrConf.xml"
TAGS_FILE = "DEF_CONF\DEF_INV_TAGS.xml"

data = {}

with open('lyrstringhelperstrings.json', 'r') as infile:
    data = json.load(infile)

end_armor_string = ""
end_armor_tags = ""
end_armor_icons = ""
cur_subfilter = BIGGEST_SUBFILTER


# Use the .json to populate .xml data (it's not the 'proper way' but I fucking hate parsing .xml so suck my dick)

for i, v in data["armor_slots"].items():
    end_armor_string += f"\n\n{data['main_string'][0]}{i}{data['main_string'][1]}(_{i}) {i}: {v}{data['main_string'][2]}{0}{data['main_string'][3]}"

for i, v in data["armor_icons"].items():
    end_armor_tags += f"\n{data['tag_string'][0]}{v}{data['tag_string'][1]}{i}{data['tag_string'][2]}"

for i, v in data["armor_icons"].items():
    end_armor_icons += f"\n{data['tag_string'][0]}{v}{data['tag_string'][1]}_{i}{data['tag_string'][2]}"

with open(LYR_FILE, "r+", encoding = "utf-8") as file:
    # Cut out previous overwritings
    lastpos = []
    while True:
        lastpos.append(file.tell())
        a = file.readline()
        print(a)
        if "Armor Slot Rollup Filters" in a or "</subCategories>" in a:
            print("Broken: " + a)
            file.seek(lastpos[-3], os.SEEK_SET)
            print(lastpos[-3])
            file.truncate()
            break
    
    # Add in new ones
    file.write(end_armor_string)
    file.write("\n</subCategories>")
    
# Generate preamble
lyr_preamble = '\n'.join(data["lyr_config_preamble"])

# Parse file
lyr_xml = ET.parse(LYR_FILE)
lyr_xml_root = lyr_xml.getroot()


# Convert subfilter integers to work with the updated algorithm (they'll work anyways, but this ensures additions don't break the .xml (because I fucking hate .xml))
def update_subfilters(xml_root):
    i = 1
    for v in xml_root.findall('./subCategory/subFilter'):
        v.text = str((i + 1) << 16)
        i += 1

# Overwrite indices with better ones
update_subfilters(lyr_xml_root)

# Add a comment in the middle (for parsing, and also to make it look it better)
comment_index = 0
for i, v in enumerate(lyr_xml_root.findall("subCategory")):
    if '30' in v.attrib["tag"]:
        comment_index = i

lyr_xml_root.insert(comment_index, ET.Comment("Armor Slot Rollup Filters\n\n--><!--"))

with open(LYR_FILE, 'w') as lyr_file:
    # Overwrite the lyrConf file, including the preamble
    lyr_file.write(lyr_preamble + "\n\n")
    lyr_file.write(str(ET.tostring(lyr_xml_root, encoding="unicode")))

with open(TAGS_FILE, "r+", encoding = "utf-8") as file:
    # Find the part of the file to cut out, then cut it out
    lastpos = []
    while True:
        lastpos.append(file.tell())
        a = file.readline()
        print(a)
        if "Armor Slot Inventory" in a:
            print("Broken: " + a)
            file.seek(lastpos[-3], os.SEEK_SET)
            print(lastpos[-3])
            file.truncate()
            break
        if "</tags>" in a:
            print("Broken: " + a)
            file.seek(lastpos[-2], os.SEEK_SET)
            print(lastpos[-2])
            file.truncate()
            break

    # Add the updated tags
    file.write(f"\n<!-- Armor Slot Inventory Tags -->{end_armor_icons}\n\n<!-- Armor Slot Rollup Tags -->{end_armor_tags}\n</tags>")
