# Slot-based Armor Sorter

A neat little DEF_UI addon that sorts each piece of armor into an individual rollup based on its slot.

## Requirements

- `DEF_UI`
- `DEF_UI Iconlibs Rescaled and Fixed`
- `FO4Edit`
- `MXPF`

## The Moving Parts

### Necessary Files

- `PipboyMenu.swf` and `PipboyInvPage.swf` - DEF_UI files, modified to increase the total number of possible rollups from 15 to 65,535 (no performance impact).
- `lyrConf.xml` - The XML file that defines roll-ups. If you're using Ruddy's Sorter, you will not need to change it. Otherwise, you'll need to run scripts.
- `DEF_CONF\DEF_INV_TAGS.xml` - The XML file that defines icons and tags. If you're using Ruddy's Sorter, you will not need to change it. Otherwise, again, you'll need to run scripts.

### Scripts to Help

- `lyrconfighelper.py` - Script to update the listed XMLs with proper rollup tags.
- `R88_SimpleSorter_modified.pas` - Script that will create a new .esp (put at the bottom of the load order, especially below any other sorter mods) with slot prefixes for each piece of clothing. Compatible with everything.

### The Extras

- `ListFiltererDecompiledEdited.txt` - The changes to the `ListFilter` script I made (for `PipboyMenu.swf` and `PipboyInvPage.swf`).
- `lyrstringhelperstrings.json` - `lyrconfighelper.py` uses this file to properly populate the XMLs.

## How to Use

1. Install mod as normal through your Mod Installer.
2. Drop `R88_SimpleSorter_modified.pas` into `FO4Edit\Edit Scripts` (generally will be in `Documents\My Games\`).
3. Open FO4Edit, and run the script. Exit, and enable the new ESP in your chosen Mod Installer.
4. If you're using Ruddy's Sorter, you should be all set!
5. Otherwise, replace `lyrConf.xml` and `DEF_CONF\DEF_INV_TAGS.xml` in this mod with the ones from your chosen sorting mod.
6. Then, run `lyrconfighelper.py`. This will update these XML files without removing any records except for those from this mod.
